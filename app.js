//
// ITERACION 1
//
// Crea una arrow function que tenga dos parametros a y b y que por defecto el valor de a = 10 y de b = 5. Haz que la función muestre por consola la suma de los dos parametros.
const funSuma = (a = 10, b = 5) => {
    console.log(a + b);
};
// 1.1 Ejecuta esta función sin pasar ningún parametro
funSuma();
// 1.2 Ejecuta esta función pasando un solo parametro
funSuma(3);
// 1.3 Ejecuta esta función pasando dos parametros
funSuma(4, 8);
//
// ITERACION 2
//
// 2.1 En base al siguiente javascript, crea variables en base a las propiedades del objeto usando object destructuring e imprimelas por consola. Cuidado, no hace falta hacer destructuring del array, solo del objeto.
const game = { title: 'The last us 2', gender: ['action', 'zombie', 'survival'], year: 2020 };
var gameTitle = game.title;
var gameGender = game.gender;
var gameYear = game.year;
console.log(gameTitle, gameGender, gameYear);
// 2.2 En base al siguiente javascript, usa destructuring para crear 3 variables llamadas fruit1, fruit2 y fruit3, con los valores del array. Posteriormente imprimelo por consola.
const fruits = ['Banana', 'Strawberry', 'Orange'];
var fruit1 = fruits[0];
var fruit2 = fruits[1];
var fruit3 = fruits[2];
console.log(fruit1, fruit2, fruit3);
// 2.3 En base al siguiente javascript, usa destructuring para crear 2 variables igualandolo a la función e imprimiendolo por consola.
// SIN TRANSFORMAR
// const animalFunction = () => {
//     return {name: 'Bengal Tiger', race: 'Tiger'}
// };
// TRANSFORMADA
const animalFunction = (name, race) => {
    return { name: name, race: race };
};
let animal = animalFunction('Bengal Tiger', 'Tiger');
console.log(animal);
// 2.4 En base al siguiente javascript, usa destructuring para crear las variables name y itv con sus respectivos valores. Posteriormente crea 3 variables usando igualmente el destructuring para cada uno de los años y comprueba que todo esta bien imprimiendolo.
const car = { name: 'Mazda 6', itv: [2015, 2011, 2020] };
var name = car.name;
var itv = car.itv;
var itv1 = car.itv[0];
var itv2 = car.itv[1];
var itv3 = car.itv[2];
console.log(itv1, itv2, itv3);
//
// ITERACION 3
//
// 3.1 Dado el siguiente array, crea una copia usando spread operators.
const pointsList31 = [32, 54, 21, 64, 75, 43];
const pointsListCopy = [...pointsList31];
console.log(pointsListCopy);
// 3.2 Dado el siguiente objeto, crea una copia usando spread operators.
const toy32 = { name: 'Bus laiyiar', date: '20-30-1995', color: 'multicolor' };
const toyCopy = { ...toy32 };
console.log(toyCopy);
// 3.3 Dado los siguientes arrays, crea un nuevo array juntandolos usando spread operatos.
const pointsList331 = [32, 54, 21, 64, 75, 43];
const pointsList332 = [54, 87, 99, 65, 32];
const pointsListJoin = [...pointsList331, ...pointsList332];
console.log(pointsListJoin);
// 3.4 Dado los siguientes objetos. Crea un nuevo objeto fusionando los dos con spread operators.
const toy34 = { name: 'Bus laiyiar', date: '20-30-1995', color: 'multicolor' };
const toyUpdate = { lights: 'rgb', power: ['Volar like a dragon', 'MoonWalk'] };
const toyFusion = { ...toy34, ...toyUpdate };
console.log(toyFusion);
// 3.5 Dado el siguiente array. Crear una copia de él eliminando la posición 2 pero sin editar el array inicial. De nuevo, usando spread operatos.
const colors = ['rojo', 'azul', 'amarillo', 'verde', 'naranja'];
const colorsCopy = [...colors.splice(0, 2), ...colors.splice(1, 2)];
console.log(colorsCopy);
// 4.1 Dado el siguiente array, devuelve un array con sus nombres utilizando .map().
const users = [
    { id: 1, name: 'Abel' },
    { id: 2, name: 'Julia' },
    { id: 3, name: 'Pedro' },
    { id: 4, name: 'Amanda' },
];
const usersName = users.map(user => user.id);
console.log(usersName);
// 4.2 Dado el siguiente array, devuelve una lista que contenga los valores de la propiedad .name y cambia el nombre a 'Anacleto' en caso de que empiece por 'A'.
const usersMap = users.map(user => {
    if (user.name.includes('A')) {
        user.name = 'Anacleto';
    }
    return user;
});
console.log(usersMap);
// 4.3 Dado el siguiente array, devuelve una lista que contenga los valores de la propiedad .name y añade al valor de .name el string ' (Visitado)' cuando el valor de la propiedad isVisited = true.
const cities = [
    { isVisited: true, name: 'Tokyo' },
    { isVisited: false, name: 'Madagascar' },
    { isVisited: true, name: 'Amsterdam' },
    { isVisited: false, name: 'Seul' },
];
const citiesVisit = cities.map(city => {
    if (city.isVisited) {
        city.name = city.name + ' (Visitado)';
    }
    return city;
});
console.log(citiesVisit);
//
// ITERACION 5
//
// 5.1 Dado el siguiente array, utiliza .filter() para generar un nuevo array con los valores que sean mayor que 18
const ages51 = [22, 14, 24, 55, 65, 21, 12, 13, 90];
const newAges = ages51.filter(num => {
    return num > 18;
});
console.log(newAges);
// 5.2 Dado el siguiente array, utiliza .filter() para generar un nuevo array con los valores que sean par.
const ages52 = [22, 14, 24, 55, 65, 21, 12, 13, 90];
const parAges = ages52.filter(num => {
    return num % 2 === 0;
});
console.log(parAges);
// 5.3 Dado el siguiente array, utiliza .filter() para generar un nuevo array con los streamers que tengan el gameMorePlayed = 'League of Legends'.
const streamers = [
    { name: 'Rubius', age: 32, gameMorePlayed: 'Minecraft' },
    { name: 'Ibai', age: 25, gameMorePlayed: 'League of Legends' },
    { name: 'Reven', age: 43, gameMorePlayed: 'League of Legends' },
    { name: 'AuronPlay', age: 33, gameMorePlayed: 'Among Us' },
];
const lolStreamers = streamers.filter(streamer => {
    if (streamer.gameMorePlayed.includes('League of Legends')) {
        return streamer;
    }
});
console.log(lolStreamers);
// 5.4 Dado el siguiente array, utiliza .filter() para generar un nuevo array con los streamers que incluyan el caracter 'u' en su propiedad .name. Recomendamos usar la funcion .includes() para la comprobación.
const uStreamers = streamers.filter(streamer => {
    if (streamer.name.includes('u')) {
        return streamer;
    }
});
console.log(uStreamers);
// 5.5 utiliza .filter() para generar un nuevo array con los streamers que incluyan el caracter 'Legends' en su propiedad .gameMorePlayed. Recomendamos usar la funcion .includes() para la comprobación.Además, pon el valor de la propiedad .gameMorePlayed a MAYUSCULAS cuando .age sea mayor que 35.
const mayusStreamers = streamers.filter(streamer => {
    if (streamer.gameMorePlayed.includes('Legends') && streamer.age > 35) {
        streamer.gameMorePlayed = streamer.gameMorePlayed.toUpperCase();
        return streamer;
    } else if (streamer.gameMorePlayed.includes('Legends')) {
        return streamer;
    }
});
console.log(mayusStreamers);
// 5.6 Dado el siguiente html y javascript, utiliza .filter() para mostrar por consola los streamers que incluyan la palabra introducida en el input. De esta forma, si introduzco 'Ru' me deberia de mostrar solo el streamer 'Rubius'. Si introduzco 'i', me deberia de mostrar el streamer 'Rubius' e 'Ibai'.
// 5.7 Dado el siguiente html y javascript, utiliza .filter() para mostrar por consola los streamers que incluyan la palabra introducida en el input. De esta forma, si introduzco 'Ru' me deberia de mostrar solo el streamer 'Rubius'. Si introduzco 'i', me deberia de mostrar el streamer 'Rubius' e 'Ibai'. En este caso, muestra solo los streamers filtrados cuando hagamos click en el button.

window.onload = () => {
    addEventListeners();
};

function addEventListeners() {
    // 5.6
    const inputText = document.querySelector('[data-function="toFilterStreamers"]');
    inputText.addEventListener('input', e => {
        const streamers561 = streamers.filter(streamer => {
            if (streamer.name.includes(e.data)) {
                return streamer.name;
            }
        });
        console.log(streamers561);
    });
    // 5.7
    const inputFilterText = document.querySelector('[data-function="toShowFilterStreamers"]');
    inputFilterText.addEventListener('click', e => {
        const streamers562 = streamers.filter(streamer => {
            if (streamer.name.includes(inputText.value)) {
                return streamer.name;
            }
        });
        console.log(streamers562);
    });
}
//
// ITERACION 6
//
// 6.1 Dado el siguiente array, usa .find() para econtrar el número 100.
const numbers = [32, 21, 63, 95, 100, 67, 43];
const found100 = numbers.find(num => num === 100);
console.log(found100);
// 6.2 Dado el siguiente array, usa .find() para econtrar la pelicula del año 2010.
const movies = [
    { title: 'Madagascar', stars: 4.5, date: 2015 },
    { title: 'Origen', stars: 5, date: 2010 },
    { title: 'Your Name', stars: 5, date: 2016 },
];
const found2010 = movies.find(movie => movie.date === 2010);
console.log(found2010);
// 6.3 Dado el siguiente javascript, usa .find() para econtrar el alien de nombre 'Cucushumushu' y la mutación 'Porompompero'. Una vez que los encuentres, usa spread operator para fusionarlos teniendo en cuenta que el objeto de la mutación lo queremos meter en la propiedad .mutation del objeto fusionado.
const aliens = [
    { name: 'Zalamero', planet: 'Eden', age: 4029 },
    { name: 'Paktu', planet: 'Andromeda', age: 32 },
    { name: 'Cucushumushu', planet: 'Marte', age: 503021 },
];
const mutations = [
    { name: 'Porompompero', description: 'Hace que el alien pueda adquirir la habilidad de tocar el tambor' },
    { name: 'Fly me to the moon', description: 'Permite volar, solo y exclusivamente a la luna' },
    { name: 'Andando que es gerundio', description: 'Invoca a un señor mayor como Personal Trainer' },
];
const mutation = [];
const cucu = aliens.find(alien => alien.name === 'Cucushumushu');
const poro = mutations.find(mutation => mutation.name === 'Porompompero');
const fusion = { ...cucu, ...poro };
mutation.push(fusion);
console.log(mutation);
//
// ITERACION 7
//
// 7.1 Dado el siguiente array, haz una suma de todos las notas de los examenes de los alumnos usando la función .reduce().
const exams = [
    { name: 'Yuyu Cabeza Crack', score: 5 },
    { name: 'Maria Aranda Jimenez', score: 1 },
    { name: 'Cristóbal Martínez Lorenzo', score: 6 },
    { name: 'Mercedez Regrera Brito', score: 7 },
    { name: 'Pamela Anderson', score: 3 },
    { name: 'Enrique Perez Lijó', score: 6 },
    { name: 'Pedro Benitez Pacheco', score: 8 },
    { name: 'Ayumi Hamasaki', score: 4 },
    { name: 'Robert Kiyosaki', score: 2 },
    { name: 'Keanu Reeves', score: 10 },
];
let suma = exams.reduce((acc, exam) => {
    return (acc += exam.score);
}, 0);
console.log(suma);
// 7.2 Dado el mismo array, haz una suma de todos las notas de los examenes de los alumnos que esten aprobados usando la función .reduce().
let sumaAp = exams.reduce((acc, exam) => {
    if (exam.score >= 5) {
        acc += exam.score;
    }
    return acc;
}, 0);
console.log(sumaAp);
// 7.3 Dado el mismo array, haz la media de las notas de todos los examenes .reduce().
let examsMedia = exams.reduce((acc, exam) => {
    return (acc += exam.score / exams.length);
}, 0);
console.log(examsMedia);
// 8.1 Dado el siguiente javascript filtra los videojuegos por gender = 'RPG' usando .filter() y usa .reduce() para conseguir la media de sus .score. La función .find() también podría ayudarte para el contrar el genero 'RPG' en el array .gender.
const videogames = [
    { name: 'Final Fantasy VII', genders: ['RPG'], score: 9.5 },
    { name: 'Assasins Creed Valhala', genders: ['Aventura', 'RPG'], score: 4.5 },
    { name: 'The last of Us 2', genders: ['Acción', 'Aventura'], score: 9.8 },
    { name: 'Super Mario Bros', genders: ['Plataforma'], score: 8.5 },
    { name: 'Genshin Impact', genders: ['RPG', 'Aventura'], score: 7.5 },
    { name: 'Legend of Zelda: Breath of the wild', genders: ['RPG', 'La cosa más puto bonita que he visto nunca'], score: 10 },
];
